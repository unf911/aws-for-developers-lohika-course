1. Upload needed  jars to s3
`./init-s3.sh`
   
2. Initialize terraform 
`terraform init`
   
3. Create infrastucture
`./create-terraform.sh`
   
4. Copy lb from script output
`e.g. tf-lb-20210708181836646800000002-1586106757.us-west-2.elb.amazonaws.com`
   
5. Replace existing lb in script that creates load on infrastructure
`nano start-client.sh`  
`replace previous lb with new one`
   
6. Start load
`./start-client.sh`
   
7. Check output. Make sure 2 ips are being used interchangeably.
```
Request: name=TG7PE, 27 + 50
Response: result=77, datetime=2021-07-08T18:35:17.676, ip=34.219.212.93

Request: name=TG7PE, 29 + 34
Response: result=63, datetime=2021-07-08T18:35:19.981, ip=35.166.50.129
```

8. Kill one ec2 box out of two in aws ui. Note that output has only one ip now

9. Go to Amazon SQS > Queues > edu-lohika-training-aws-sqs-queue > Send and receive messages. 
   Make sure message are being sent to queue
  
10. Upload private key to bastion.
`scp -i "keypair_msi_ec2_anefodov_at_lohika_com.pem" keypair_msi_ec2_anefodov_at_lohika_com.pem  ec2-user@54.68.121.182:~`
    
11. Ssh to bastion. Restrict access to private key. Ssh to persister.
`chmod 400 keypair_msi_ec2_anefodov_at_lohika_com.pem`
`ssh -i "keypair_msi_ec2_anefodov_at_lohika_com.pem" ec2-user@10.0.3.122`
    
12. Verify that persister can see internet
`ping 8.8.8.8`
    
13. Verify that persister can see rds
`psql -h postgres-rds-week6.c7epdx7kwq61.us-west-2.rds.amazonaws.com -U rootuser -d EduLohikaTrainingAwsRds`
`select * from logs;`
    



