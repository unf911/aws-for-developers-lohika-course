#!/usr/bin/env bash

aws s3 mb s3://anefodov-lohika-com-test6-bucket --region us-west-2 --profile anefodov@lohika.com
aws s3api put-bucket-versioning --bucket anefodov-lohika-com-test6-bucket --versioning-configuration Status=Enabled --profile anefodov@lohika.com

aws s3api put-public-access-block \
    --bucket anefodov-lohika-com-test6-bucket \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true" \
    --profile anefodov@lohika.com

aws s3 cp calc-0.0.2-SNAPSHOT.jar s3://anefodov-lohika-com-test6-bucket \
    --profile anefodov@lohika.com

aws s3 cp persist3-0.0.1-SNAPSHOT.jar s3://anefodov-lohika-com-test6-bucket \
    --profile anefodov@lohika.com
