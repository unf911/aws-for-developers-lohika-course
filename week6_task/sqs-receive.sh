#!/usr/bin/env bash

aws sqs receive-message \
  --queue-url https://sqs.us-west-2.amazonaws.com/729871128771/edu-lohika-training-aws-sqs-queue  \
  --region us-west-2 \
  --profile anefodov@lohika.com
