variable "instance_type" {
  default = "t2.micro"
}
variable "region" {
  default = "us-west-2"
}

variable "key_name" {
  default = "keypair_msi_ec2_anefodov_at_lohika_com"
}