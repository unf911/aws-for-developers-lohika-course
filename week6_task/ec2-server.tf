terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "anefodov@lohika.com"
  region = var.region
}

resource "aws_security_group" "application-server-sg" {
  name = "application_server"
  description = "Allow ssh and http to ec2"

  depends_on = [aws_vpc.week6-vpc]
  vpc_id      = aws_vpc.week6-vpc.id

  ingress {
    description = "ssh"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "https"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_iam_role" "s3-full-access-for-ec2-role" {
  name = "s3-full-access-for-ec2-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow",
        Action = "sts:AssumeRole"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }],
  })
}


resource "aws_iam_policy" "s3-sqs-policy" {
  name = "s3-sqs-policy"
  description = "A policy allowing full access to s3 and sqs"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        Effect : "Allow",
        Action : "s3:*",
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sqs:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "dynamodb:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sns:*"
        Resource : "*"
      }
    ]
  })
}


resource "aws_iam_policy_attachment" "s3-attachment" {
  name = "test-attachment"
  roles = [aws_iam_role.s3-full-access-for-ec2-role.name]
  policy_arn = aws_iam_policy.s3-sqs-policy.arn
}

resource "aws_iam_instance_profile" "ec2_s3" {
  name = "ec2_s3"
  role = aws_iam_role.s3-full-access-for-ec2-role.name
}

#=====================
#Create sqs queue
#TODO extract into separate module
resource "aws_sqs_queue" "queue" {
  name = "edu-lohika-training-aws-sqs-queue"
}

output "sqs_queue_url" {
  description = "SQS queu URL"
  value = aws_sqs_queue.queue.id
}

# TODO extract sqs policy into its own policy and attachment

#=====================
#Create dynamodb table

resource "aws_dynamodb_table" "log-table-dynamodb" {
  name             = "edu-lohika-training-aws-dynamodb"
  hash_key         = "UserName"
  write_capacity   = 1
  read_capacity   = 1

  attribute {
    name = "UserName"
    type = "S"
  }
  # TODO extract dynamodb policy into its own policy and attachment

}

#=====================
#Create persister ec2


resource "aws_iam_policy" "rds-policy" {
  name = "rds-policy"
  description = "A policy allowing full access to rds"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        Effect : "Allow",
        Action : "rds-db:connect",
        Resource : "*"
      },
      {
        Effect : "Allow",
        Action : "s3:*",
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sqs:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sns:*"
        Resource : "*"
      }
    ]
  })
}

resource "aws_iam_role" "rds-access-for-ec2-role" {
  name = "rds-access-for-ec2-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow",
        Action = "sts:AssumeRole"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }],
  })
}

resource "aws_iam_policy_attachment" "rds-attachment" {
  name = "rds-attachment"
  roles = [aws_iam_role.rds-access-for-ec2-role.name]
  policy_arn = aws_iam_policy.rds-policy.arn
}

resource "aws_iam_instance_profile" "ec2_rds_ip" {
  name = "ec2_rds_ip"
  role = aws_iam_role.rds-access-for-ec2-role.name
}

resource "aws_security_group" "rds-security-group" {
  name = "rds-security-group"
  description = "Allow 5432"
  vpc_id = aws_vpc.week6-vpc.id

  ingress {
    description = "postgres"
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#############
#Postgres

resource "aws_db_subnet_group" "rds-subnet-group" {
  name       = "main"
  subnet_ids = [aws_subnet.private-subnet-1.id, aws_subnet.private-subnet-2.id]

  tags = {
    Name = "RDS subnet group that is using only private subnets"
  }
}

resource "aws_db_instance" "rds" {
  name = "EduLohikaTrainingAwsRds"
  identifier = "postgres-rds-week6"
  instance_class = "db.t3.micro"
  allocated_storage = 5
  engine = "postgres"
  engine_version = "12.5"
  username = "rootuser"
  password = "rootuser"
  port = "5432"
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet-group.name
  vpc_security_group_ids = [aws_security_group.rds-security-group.id]
  skip_final_snapshot = true
}

resource "aws_instance" "week6-ec2-persister" {
  ami = "ami-0cf6f5c8a62fa5da6"
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.application-server-sg.id]
  subnet_id = aws_subnet.private-subnet-1.id
  associate_public_ip_address = true

  iam_instance_profile = aws_iam_instance_profile.ec2_rds_ip.id
  tags = {
    Name = "week6-ec2-persister"
  }
  user_data = base64encode(templatefile("user-data-persister.sh", { rds_host = aws_db_instance.rds.address }))
}

############################
#SNS
resource "aws_sns_topic" "sns-topic" {
  name = "edu-lohika-training-aws-sns-topic"
}

output "rds_endpoint" {
  value = aws_db_instance.rds.endpoint
}

#################
#ALB
resource "aws_vpc" "week6-vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "week6-vpc"
  }
}

resource "aws_internet_gateway" "week6-vpc-internet-gateway" {
  vpc_id = aws_vpc.week6-vpc.id
  tags = {
    Name = "week6-vpc-internet-gateway"
  }
}


resource "aws_route_table" "allow-route-table" {
  vpc_id = aws_vpc.week6-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.week6-vpc-internet-gateway.id
  }

  tags = {
    Name = "allow-route-table"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public-subnet-1.id
  route_table_id = aws_route_table.allow-route-table.id
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.public-subnet-2.id
  route_table_id = aws_route_table.allow-route-table.id
}

data "aws_availability_zone" "us-west-2a" {
  name = "us-west-2a"
}

data "aws_availability_zone" "us-west-2b" {
  name = "us-west-2b"
}

data "aws_availability_zone" "us-west-2c" {
  name = "us-west-2c"
}

data "aws_availability_zone" "us-west-2d" {
  name = "us-west-2d"
}

resource "aws_subnet" "public-subnet-1" {
  vpc_id     = aws_vpc.week6-vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zone.us-west-2a.name
  tags = {
    Name = "public-subnet-1"
  }
}

resource "aws_subnet" "public-subnet-2" {
  vpc_id     = aws_vpc.week6-vpc.id
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zone.us-west-2b.name
  tags = {
    Name = "public-subnet-1"
  }
}

resource "aws_subnet" "private-subnet-1" {
  vpc_id     = aws_vpc.week6-vpc.id
  cidr_block = "10.0.3.0/24"
  availability_zone = data.aws_availability_zone.us-west-2c.name
  tags = {
    Name = "private-subnet-1"
  }
}

resource "aws_subnet" "private-subnet-2" {
  vpc_id     = aws_vpc.week6-vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone = data.aws_availability_zone.us-west-2d.name
  tags = {
    Name = "private-subnet-2"
  }
}

resource "aws_lb_target_group" "server-alb-tg" {
  name     = "server-alb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.week6-vpc.id
  health_check {
    path                = "/actuator/health"
    port                = 80
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 10
    matcher             = "200"
  }
}

resource "aws_lb" "lb" {
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.application-server-sg.id]
  subnets            = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
  enable_deletion_protection = false
  tags = {
    Name = "lb"
  }
}

resource "aws_lb_listener" "lb-listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.server-alb-tg.arn
  }
}

resource "aws_launch_template" "server-launch-template" {
  name = "server-launch-template"
  image_id = "ami-0cf6f5c8a62fa5da6"
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.application-server-sg.id]
  iam_instance_profile {
    name = aws_iam_instance_profile.ec2_s3.id
  }
  user_data = filebase64("user-data-server.sh")
}

resource "aws_autoscaling_group" "asg" {
  max_size           = 2
  min_size           = 2
  vpc_zone_identifier = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
  target_group_arns = [aws_lb_target_group.server-alb-tg.arn]
  launch_template {
    id      = aws_launch_template.server-launch-template.id
    version = "$Latest"
  }
}

resource "aws_autoscaling_attachment" "asg-attachment" {
  autoscaling_group_name = aws_autoscaling_group.asg.id
  alb_target_group_arn   = aws_lb_target_group.server-alb-tg.arn
}


output "alb_dns" {
  description = "DNS name of ALB resource"
  value       = aws_lb.lb.dns_name
}

################
#Bastion

resource "aws_security_group" "nat-sg" {
  depends_on = [
    aws_vpc.week6-vpc
  ]
  name        = "nat-sg"
  description = "Allow traffic to pass from the private subnet to the internet"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private-subnet-1.cidr_block, aws_subnet.public-subnet-2.cidr_block]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private-subnet-1.cidr_block, aws_subnet.public-subnet-2.cidr_block]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.week6-vpc.cidr_block]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.week6-vpc.id

  tags = {
    Name = "nat-sg"
  }
}

resource "aws_instance" "bastion-week6" {
  depends_on = [aws_security_group.nat-sg]
  ami = "ami-0032ea5ae08aa27a2"
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.nat-sg.id]
  subnet_id = aws_subnet.public-subnet-1.id
  key_name = var.key_name
  associate_public_ip_address = true
  source_dest_check = false
  tags = {
    Name = "bastion-week6"
  }
}

resource "aws_route_table" "private-subnet-rt" {
  vpc_id = aws_vpc.week6-vpc.id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.bastion-week6.id
  }

  tags = {
    Name = "private-subnet-rt"
  }
}

resource "aws_route_table_association" "private-subnet-1-association" {
  subnet_id      = aws_subnet.private-subnet-1.id
  route_table_id = aws_route_table.private-subnet-rt.id
}

resource "aws_route_table_association" "private-subnet-2-association" {
  subnet_id      = aws_subnet.private-subnet-2.id
  route_table_id = aws_route_table.private-subnet-rt.id
}