#!/usr/bin/env bash

sudo yum -y update
sudo yum install -y java-1.8.0-openjdk
aws s3 cp s3://anefodov-lohika-com-test6-bucket/calc-0.0.2-SNAPSHOT.jar /home/ec2-user/.
cd /home/ec2-user/
sudo java -jar calc-0.0.2-SNAPSHOT.jar


