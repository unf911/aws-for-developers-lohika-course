#!/usr/bin/env bash

sudo yum -y update
sudo yum install -y java-1.8.0-openjdk

sudo amazon-linux-extras install postgresql12

aws s3 cp s3://anefodov-lohika-com-test6-bucket/persist3-0.0.1-SNAPSHOT.jar /home/ec2-user/.
cd /home/ec2-user/
export RDS_HOST="${rds_host}"
echo "RDS_HOST=$RDS_HOST"
sudo -E bash -c 'java -jar persist3-0.0.1-SNAPSHOT.jar'
