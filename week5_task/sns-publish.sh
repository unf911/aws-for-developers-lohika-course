#!/usr/bin/env bash

aws sns publish \
  --topic-arn "arn:aws:sns:us-west-2:729871128771:topic-terraform-created" \
  --message file://message.txt \
  --profile anefodov@lohika.com --region us-west-2