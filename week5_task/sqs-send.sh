#!/usr/bin/env bash

aws sqs send-message \
  --queue-url https://sqs.us-west-2.amazonaws.com/729871128771/terraform-example-queue  \
  --message-body "Information about the largest city in Any Region." \
  --profile anefodov@lohika.com --region us-west-2
