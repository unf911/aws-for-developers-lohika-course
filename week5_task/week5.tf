terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "anefodov@lohika.com"
  region  = var.region
}

#Create sqs queue
resource "aws_sqs_queue" "terraform_queue" {
  name = "terraform-example-queue"
}

#Create sns topic
resource "aws_sns_topic" "topic-terraform-created" {
  name = "topic-terraform-created"
}

#Create ec2
resource "aws_instance" "week5-ec2" {
  ami           = "ami-0cf6f5c8a62fa5da6"
  instance_type = var.instance_type
  key_name = var.key_name
  tags = {
    Name = "week5-ec2"
  }
}

//output "ec2-private-ip" {
//  description = "Private ip of ec2 for sqs policy"
//  value       = aws_instance.week5-ec2.private_ip
//}

output "ec2-public-ip" {
  description = "Public ip of ec2 for external ssh access"
  value       = aws_instance.week5-ec2.public_ip
}

##Ec2 instance access to sqs


resource "aws_sqs_queue_policy" "test" {
  queue_url = aws_sqs_queue.terraform_queue.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
         "sqs:SendMessage",
         "sqs:ReceiveMessage"
      ],
      "Resource": "${aws_sqs_queue.terraform_queue.arn}",
      "Condition" : {
        "IpAddress" : {
          "aws:SourceIp":  ["${aws_instance.week5-ec2.private_ip}"]
        }
      }
    }
  ]
}
POLICY
}


##Ec2 instance access to sns
#Add output Variable with ec2 ip
#Add output variable with topic arn
#Add output variable with queue URL

#Recieve message from sqs via aws cli
#Create email subscription to SNS topic via aws ui. Confirm it.
#Send message to sns via aws cli
#Harmonize names of resources
#Remove commented out code
#Commit
#Remove comments
#Commit
