#!/usr/bin/env bash

#Display existing Dynamo DB tables
aws dynamodb list-tables --region us-west-2

#Add a few entries in the table
aws dynamodb put-item     --table-name dynamodb-table-1 --item '{
        "key": {"S": "001"},
        "name": {"S": "Name1"} ,
        "AlbumTitle": {"S": "Somewhat Famous"}
      }' --region us-west-2

aws dynamodb put-item     --table-name dynamodb-table-1 --item '{
        "key": {"S": "002"},
        "name": {"S": "Name2"} ,
        "AlbumTitle": {"S": "Infamous"}
      }' --region us-west-2


#	Read entries from the table
aws dynamodb scan --table-name dynamodb-table-1 --region us-west-2