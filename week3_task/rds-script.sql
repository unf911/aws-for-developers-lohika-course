-- example of cli connect string
-- psql -h demo-rds.c7epdx7kwq61.us-west-2.rds.amazonaws.com --username post123 --dbname demo_db

CREATE SCHEMA IF NOT EXISTS anefodov_schema;

CREATE TABLE anefodov_schema.anefodov_test (
  id integer NOT null,
  name varchar(256) NOT NULL
);

insert into anefodov_schema.anefodov_test values (1, 'Masyanya');
insert into anefodov_schema.anefodov_test (id, name) values (2, 'Jack');


select * from anefodov_schema.anefodov_test;
