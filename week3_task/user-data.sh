#!/usr/bin/env bash
echo \"UserData started\"
aws s3 cp s3://anefodov-lohika-com-test4-bucket/rds-script.sql /home/ec2-user/.
aws s3 cp s3://anefodov-lohika-com-test4-bucket/dynamodb-script.sh /home/ec2-user/.
sudo chmod a+x dynamodb-script.sh
sudo amazon-linux-extras install -y postgresql10
echo \"UserData finished\"

