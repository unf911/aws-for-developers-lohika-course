//•	Create a Terraform script which will create the next infrastructure:

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "anefodov@lohika.com"
  region  = var.region
}

resource "aws_dynamodb_table" "dynamodb-table-1" {
  name             = "dynamodb-table-1"
  hash_key         = "key"
  write_capacity   = 1
  read_capacity   = 1

  attribute {
    name = "key"
    type = "S"
  }

}
resource "aws_iam_role" "ec2_s3_access_role" {
  name = "s3-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect = "Allow",
      Action = "sts:AssumeRole"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }
    ],
  })
}

resource "aws_iam_instance_profile" "ec2_s3" {
//  name = "instance-profile-with-s3-access"
  name = "ec2_s3"
  role = aws_iam_role.ec2_s3_access_role.name
}

resource "aws_iam_policy" "policy" {
  name = "test-policy"
  description = "A test policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        Effect : "Allow",
        Action : "s3:*",
        Resource : "*"
      },
      {
        Effect : "Allow",
        Action : "rds-db:connect",
        Resource : "*"
      },
      {
        Effect : "Allow",
        Action : "dynamodb:*",
        Resource : "*"
      }
    ]
  })
}


resource "aws_iam_policy_attachment" "test-attach" {
  name = "test-attachment"
  roles = [aws_iam_role.ec2_s3_access_role.name]
  policy_arn = aws_iam_policy.policy.arn
}


resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http"
  description = "Allow ssh and http to ec2"

  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0"]
  }

}

resource "aws_instance" "AnefodovEc2S3Terraform" {
  ami           = "ami-0cf6f5c8a62fa5da6"
  instance_type = var.instance_type
  security_groups = [aws_security_group.allow_ssh_http.name]
  iam_instance_profile = aws_iam_instance_profile.ec2_s3.id
  key_name = var.key_name
  user_data = filebase64("user-data.sh")
}

resource "aws_db_instance" "demo" {
  identifier = "demo-rds"
  name = "demo_db"
  instance_class = var.db_instance_class
  allocated_storage = 10
  engine = "postgres"
  username = var.aws_db_instance__demo__username
  password = var.aws_db_instance__demo__password
  publicly_accessible = true
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.allow_ssh_http.id]
}
