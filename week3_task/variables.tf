variable "instance_type" {
  default = "t2.micro"
}
variable "region" {
  default = "us-west-2"
}

variable "key_name" {
  default = "keypair_msi_ec2_anefodov_at_lohika_com"
}

variable "db_instance_class" {
  default = "db.t2.micro"
}

variable "aws_db_instance__demo__username" {
  default = "post123"
}

variable "aws_db_instance__demo__password" {
  default = "post123$%^"
}