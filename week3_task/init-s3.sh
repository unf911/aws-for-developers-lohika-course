#!/usr/bin/env bash

aws s3 mb s3://anefodov-lohika-com-test4-bucket --region us-west-1 --profile anefodov@lohika.com
aws s3api put-bucket-versioning --bucket anefodov-lohika-com-test4-bucket --versioning-configuration Status=Enabled --profile anefodov@lohika.com

aws s3api put-public-access-block \
    --bucket anefodov-lohika-com-test4-bucket \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true" \
    --profile anefodov@lohika.com

aws s3 cp rds-script.sql s3://anefodov-lohika-com-test4-bucket/rds-script.sql \
    --profile anefodov@lohika.com
#Maybe filename for destination is redundant?
aws s3 cp dynamodb-script.sh s3://anefodov-lohika-com-test4-bucket/dynamodb-script.sh \
    --profile anefodov@lohika.com