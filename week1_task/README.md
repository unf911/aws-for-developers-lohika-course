# Creating EC2 instance via cloudformation script
## Prerequisites
  git bash on Windows or bash shell on Linux  
  aws profile with name "anefodov@lohika.com"  
  ssh key keypair_ec2_anefodov_at_lohika_com in ~/.ssh folder

  ### register CommandRunner
   https://aws.amazon.com/premiumsupport/knowledge-center/cloudformation-commandrunner-stack/

   `git clone https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-awsutilities-commandrunner.git`  
   `cd aws-cloudformation-resource-providers-awsutilities-commandrunner`  
   `curl -LO https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-awsutilities-commandrunner/releases/latest/download/awsutility-cloudformation-commandrunner.zip`  
   `./scripts/register.sh --set-default`  

## To create stack run in 
`./ec2.sh`

## To delete stack
`./delete_stack.sh`

## Example of connect string for ssh
`ssh -i "~/.ssh/keypair_ec2_anefodov_at_lohika_com.pem" ec2-user@34.212.29.36`
