terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "anefodov@lohika.com"
  region  = var.region
}

resource "aws_vpc" "my-vpc-2" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "my-vpc-2"
  }
}

data "aws_availability_zone" "us-west-2a" {
  name = "us-west-2a"
}

data "aws_availability_zone" "us-west-2b" {
  name = "us-west-2b"
}
resource "aws_subnet" "public-subnet" {
  vpc_id     = aws_vpc.my-vpc-2.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zone.us-west-2a.name
  tags = {
    Name = "public-subnet"
  }
}

resource "aws_subnet" "private-subnet" {
  vpc_id     = aws_vpc.my-vpc-2.id
  cidr_block = "10.0.2.0/24"
  availability_zone = data.aws_availability_zone.us-west-2b.name
  tags = {
    Name = "private-subnet"
  }
}

resource "aws_internet_gateway" "my-internet-gateway-2" {
  vpc_id = aws_vpc.my-vpc-2.id

  tags = {
    Name = "my-internet-gateway-2"
  }
}

resource "aws_route_table" "my-route-table-2" {
  vpc_id = aws_vpc.my-vpc-2.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-internet-gateway-2.id
  }

  tags = {
    Name = "my-route-table-2"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.my-route-table-2.id
}


resource "aws_security_group" "allow_ssh_http" {
  depends_on = [
    aws_vpc.my-vpc-2
  ]
  name        = "allow_ssh_http"
  description = "Allow ssh and http to ec2"
  vpc_id      = aws_vpc.my-vpc-2.id
  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0"]
  }

}

resource "aws_instance" "week4-public-ec2" {
  depends_on = [
    aws_security_group.allow_ssh_http
  ]
  ami           = "ami-0cf6f5c8a62fa5da6"
  instance_type = var.instance_type
  subnet_id = aws_subnet.public-subnet.id
  vpc_security_group_ids  = [aws_security_group.allow_ssh_http.id]
  key_name = var.key_name
  user_data = filebase64("user-data-public.sh")
  tags = {
    Name = "week4-public-ec2"
  }
}


resource "aws_security_group" "allow_icmp_ssh_http_for_subnet1" {
  depends_on = [
    aws_vpc.my-vpc-2
  ]
  name        = "allow_icmp_ssh_http_for_subnet1"
  description = "allow_icmp_ssh_http_for_subnet1"
  vpc_id      = aws_vpc.my-vpc-2.id
  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["10.0.1.0/24"]
  }
  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["10.0.1.0/24"]
  }
  ingress {
    description      = "icmp"
    from_port = 8
    to_port = 0
    protocol = "icmp"
    cidr_blocks = ["10.0.1.0/24"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0"]
  }

}

resource "aws_instance" "week4-private-ec2" {
  depends_on = [
    aws_security_group.allow_icmp_ssh_http_for_subnet1,
  ]
  ami           = "ami-0cf6f5c8a62fa5da6"
  instance_type = var.instance_type
  subnet_id = aws_subnet.private-subnet.id
  vpc_security_group_ids       = [aws_security_group.allow_icmp_ssh_http_for_subnet1.id]
  key_name = var.key_name
  user_data = filebase64("user-data-private.sh")
  tags = {
    Name = "week4-private-ec2-1"
  }
}


resource "aws_security_group" "allow_icmp_ssh_http_for_subnet2" {
  name        = "allow_icmp_ssh_http_for_subnet2"
  description = "Allow incoming SSH/HTTP connections."

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.my-vpc-2.id

  tags = {
    Name = "allow_icmp_ssh_http_for_subnet2"
  }
}

resource "aws_security_group" "nat_sg" {
  depends_on = [
      aws_vpc.my-vpc-2
  ]
  name        = "nat_sg"
  description = "Allow traffic to pass from the private subnet to the internet"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private-subnet.cidr_block]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private-subnet.cidr_block]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.my-vpc-2.cidr_block]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.my-vpc-2.id

  tags = {
    Name = "nat_sg"
  }
}


resource "aws_instance" "week4-nat-2" {
  depends_on = [
    aws_security_group.nat_sg
  ]
  ami           = "ami-0032ea5ae08aa27a2"
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.nat_sg.id]
  subnet_id = aws_subnet.public-subnet.id
  key_name = var.key_name
  associate_public_ip_address = true
  source_dest_check = false
  tags = {
    Name = "week4-nat-2"
  }
}

resource "aws_route_table" "private_subnet_route_table" {
  vpc_id = aws_vpc.my-vpc-2.id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.week4-nat-2.id
  }

  tags = {
    Name = "Private Subnet"
  }
}

resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = aws_subnet.private-subnet.id
  route_table_id = aws_route_table.private_subnet_route_table.id
}

resource "aws_lb_target_group" "alb-tg" {
  name     = "alb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.my-vpc-2.id
    health_check {
      path                = "/"
      port                = 80
      healthy_threshold   = 5
      unhealthy_threshold = 2
      timeout             = 2
      interval            = 10
      matcher             = "200"
    }
}

resource "aws_lb" "lb" {
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_ssh_http.id]
  subnets            = [aws_subnet.public-subnet.id, aws_subnet.private-subnet.id]
  enable_deletion_protection = false
  tags = {
    Name = "lb"
  }
}

resource "aws_lb_listener" "lb-listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-tg.arn
  }
}

resource "aws_lb_target_group_attachment" "tga-public" {
  target_group_arn = aws_lb_target_group.alb-tg.arn
  target_id        = aws_instance.week4-public-ec2.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "tga-private" {
  target_group_arn = aws_lb_target_group.alb-tg.arn
  target_id        = aws_instance.week4-private-ec2.id
  port             = 80
}

output "alb_dns" {
  description = "DNS name of ALB resource"
  value       = aws_lb.lb.dns_name
}
