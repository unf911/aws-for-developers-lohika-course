#!/bin/bash

sudo yum update -y
sudo yum install -y httpd.x86_64
sudo systemctl start httpd.service
sudo systemctl enable httpd.service
cd /var/www/html
echo "<html><h1>This is WebServer from public subnet</h1></html>" > index.html
