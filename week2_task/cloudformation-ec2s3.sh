#!/usr/bin/env bash

aws --region us-west-2 cloudformation create-stack --stack-name anefodov-week2 \
  --capabilities CAPABILITY_IAM \
  --template-body file://ec2s3.yaml --profile anefodov@lohika.com