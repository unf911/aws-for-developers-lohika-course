# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.43.0"
  constraints = "~> 3.27"
  hashes = [
    "h1:ruZxfmi8+HE18fvjm3+1Ujawcoah1Am7Dk78vlJIb30=",
    "zh:1496d971301216bfd27aada08f83315748972a50782c3c7a998212d733a8bd4f",
    "zh:3f43fd130eaf6ff82d713add40d38a526e978975e9517defabb32fe056a32371",
    "zh:52db549bf4d77235beb01c7bba72d577aa141a812cc1045a2808b40d2262fc3d",
    "zh:5ebdcedc4057d65e2d5689d15534eb8b4d175d8138952a298fba1c3e881c01c5",
    "zh:6264aacffd2caf82eabde2f3298cfed44377a2839dd88c67c860b83589c15129",
    "zh:759a6993c6692fd19ae83add4fd11c6be4e74adb5e7a02baff278386d4a89990",
    "zh:8a5975e90215a6c7af4eddf6fcdffb8e4aab4ff7885409728d78fff6c9e37235",
    "zh:8dd00c37cf496487066129ed19a0f7eae090cef333251789d945bc35c1723ab6",
    "zh:a0b615859497deeb95d09336a4a0c87e3687092188950b029434f742928fd299",
    "zh:e7d588099ec1868fd419cce7dc54c717816d3cb2206cc9564b6ace2a82d14f79",
    "zh:faf8443d3f87fc41d20a5867c5efecaa2c56d97b83c8f30c485c3b5dd4b7a226",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:LN84cu+BZpVRvYlCzrbPfCRDaIelSyEx/W9Iwwgbnn4=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
