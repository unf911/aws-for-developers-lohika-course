#Week2 task

Run `init-s3.sh` to upload file to s3
Run `cloudformation-ec2s3.sh` to create needed infrastructure via cloudformation
Run `cloudformation-delete-stack.sh` to delete infrastructure
Run `week2_task/week2_terraform/week2-terraform.sh` to create needed infrastructure via terraform
Run `week2_task/week2_terraform/week2-terraform-delete.sh` to delete needed stack
Run `week2_task/week1_terraform/week1-terraform.sh` to create infrastructure for week1 AND Week0
Run `week2_task/week1_terraform/week1-terraform-delete.sh` to delete infra for week0, week1