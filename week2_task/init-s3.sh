#!/usr/bin/env bash

aws s3 mb s3://anefodov-lohika-com-test3-bucket --region us-west-1 --profile anefodov@lohika.com
aws s3api put-bucket-versioning --bucket anefodov-lohika-com-test3-bucket --versioning-configuration Status=Enabled --profile anefodov@lohika.com

aws s3api put-public-access-block \
    --bucket anefodov-lohika-com-test3-bucket \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true" \
    --profile anefodov@lohika.com

aws s3 cp test-file-to-upload-to-s3.txt s3://anefodov-lohika-com-test3-bucket/test-file-to-upload-to-s3.txt \
    --profile anefodov@lohika.com