terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "anefodov@lohika.com"
  region  = "us-west-2"
}

data "template_file" "test" {
  template = <<EOF
    #!/bin/bash
    sudo yum install -y java-1.8.0-openjdk
  EOF
}

resource "aws_launch_template" "anef_launch_template_tf" {
  name = "anef_launch_template_tf"

  image_id             = "ami-0cf6f5c8a62fa5da6"
  instance_type        = "t2.micro"
  security_group_names = ["http_ssh_allowed"]
  key_name             = "keypair_msi_ec2_anefodov_at_lohika_com"
  user_data            = base64encode(data.template_file.test.rendered)
}


resource "aws_security_group" "http_ssh_allowed" {
  name        = "http_ssh_allowed"
  description = "Allow http and ssh traffic"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_ssh"
  }
}

resource "aws_autoscaling_group" "anef_asg_tf" {
  availability_zones = ["us-west-2a", "us-west-2b"]
  desired_capacity   = 2
  max_size           = 2
  min_size           = 2

  launch_template {
    id      = aws_launch_template.anef_launch_template_tf.id
    version = "$Latest"
  }
}

